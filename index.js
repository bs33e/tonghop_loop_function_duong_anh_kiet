function inBangSo() {
  var content = "";
  var contentHTML = "";

  for (var x = 1; x <= 100; x += 10) {
    for (var y = 0; y < 10; y++) {
      var num = x + y;
      content = num + " | ";
      contentHTML += content;

      if (num >= 100) {
        break;
      }
    }
    contentHTML += "<br>";
  }
  document.getElementById("xuatBangSo").innerHTML = contentHTML;
}

var arrNum = [];

function nhapSo() {
  if (document.querySelector("#number").value == "") {
    return;
  }
  if (Number.isInteger(document.querySelector("#number").value * 1) == false) {
    return;
  }
  var number = document.querySelector("#number").value * 1;
  arrNum.push(number);
  document.querySelector("#number").value = "";
  document.querySelector("#result").innerHTML = "Mảng: " + arrNum;
  timSoNguyenTo();
}

function kiemTraSnt(n) {
  var flag = 1;
  if (n < 2) return (flag = 0);
  var i = 2;
  while (i < n) {
    if (n % i == 0) {
      flag = 0;
      break;
    }
    i++;
  }
  return flag;
}

function timSntDauTien(arr) {
  var result;
  var content = "";
  for (var index = 0; index < arr.length; index++) {
    if (kiemTraSnt(arr[index]) == 1) {
      result = arr[index] + "  ";
      content += result;
    }
  }
  return content;
}

function timSoNguyenTo() {
  var timSoNguyenToTrongMang = timSntDauTien(arrNum);
  document.querySelector("#resultSnt").innerHTML =
    "Các số nguyên tố là: " + timSoNguyenToTrongMang;
}

function tinhS() {
  var SoN = document.querySelector("#nhapSoN").value * 1;
  console.log("SoN: ", SoN);
  document.querySelector("#nhapSoN").value = "";

  var sum = 0;
  for (var i = 2; i <= SoN; i++) {
    sum += i;

    if (i == SoN) {
      sum += 2 * SoN;
    }
  }

  document.querySelector("#xuatKetQuaTinhS").innerHTML = "S = " + sum;
}

function tinhLuongUocSo() {
  var soN = document.querySelector("#nhapThamSoN").value * 1;
  console.log("SoN: ", soN);
  var uocSo;
  var content = "";
  for (var i = soN; i >= 1; i--) {
    if (soN % i == 0) {
      uocSo = i + " ";
      content += uocSo;
    }
  }

  console.log("content: ", content);
  document.querySelector("#xuatKetQuaUocSo").innerHTML =
    "Ươc số của " + soN + " là: " + content;
}
function duyetChuoi(str) {
  var content = "";
  for (var index = str.length - 1; index >= 0; index--) {
    var result = str.charAt(index);
    content += result;
  }
  return content;
}

function timSoDaoNguoc() {
  var inputNumber = document.querySelector("#inputNumber").value;
  console.log("inputNumber: ", inputNumber);
  var tinhToanChuoi = duyetChuoi(inputNumber);
  document.querySelector("#xuatKetQuaSoDaoNguoc").innerHTML = tinhToanChuoi;
}

function timX() {
  var result;
  var i = 1;
  for (var x = 0; x < 100; x++) {
    i += x;
    if (i <= 100) {
      result = x;
    }
  }

  document.querySelector("#xuatGiaTriX").innerHTML = "Giá trị x là: " + result;
}

function inBangCuuChuong() {
  if (document.querySelector("#inputSoN").value == "") {
    return;
  }
  if (
    Number.isInteger(document.querySelector("#inputSoN").value * 1) == false
  ) {
    return;
  }

  if (document.querySelector("#inputSoN").value * 1 > 9) {
    return;
  }
  var inputSoN = document.querySelector("#inputSoN").value * 1;

  console.log("inputSoN: ", inputSoN);
  document.querySelector("#inputSoN").value = "";

  var content ="";
  var contentHTML = "";
  var nhanHaiSo;
  for (var i =1; i <= 10; i++) {
    nhanHaiSo = inputSoN * i;
    content = inputSoN + " x " + i + " = " + nhanHaiSo;
    contentHTML+= content;
    contentHTML+= `<br>`;

  }
  
  
  document.querySelector("#xuatBangCuuChuong").innerHTML= contentHTML;

}

var players =[[],[],[],[]];

var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", "AS", "7H", "9K", "10D"];

function chiaBai(arr) {

  for (var step =0; step<4; step++) {
    for(var index=step; index <= arr.length-1; index+=4) {
      players[step].push(arr[index]);
  
    }
  }
  return players;
}


function chiaBaiChoNguoiChoi() {
  chiaBai (cards);
  document.querySelector("#xuatKetQuaChiaBai").innerHTML = `
  <p>Player1: ${players[0]}</p>
  <p>Player2: ${players[1]}</p>
  <p>Player3: ${players[2]}</p>
  <p>Player4: ${players[3]}</p>

  `;
  
}

var x; // số lượng chân gà
var y; // số lượng chân chó
var soGa;
var soCho;


function timSoGaVaCho() {
  var nhapM = document.querySelector("#nhapNumberM").value*1;
  console.log('nhapM: ', nhapM);
  var nhapN = document.querySelector("#nhapNumberN").value*1;
  console.log('nhapN: ', nhapN);
  
  x = 4*nhapM - nhapN;
  soGa = x/2;
  soCho = nhapM - soGa;
  
  document.querySelector("#xuatKetQuaSoGaCho").innerHTML= `<p>Số gà là: ${soGa}, Số chó là: ${soCho}</p>`;

}

function timGocLech() {
  if (document.querySelector("#nhapGio").value*1 > 12) {
    return;
  }
  if (document.querySelector("#nhapPhut").value*1 > 60) {
    return;
  }
  var soGio = document.querySelector("#nhapGio").value*1;
  console.log('soGio: ', soGio);

  var soPhut = document.querySelector("#nhapPhut").value*1;
  console.log('soPhut: ', soPhut);

  var toaDoGio = soGio*30;
  var toaDoPhut = soPhut*6;
  if (soGio == 0 || soGio == 12) {
    toaDoGio = 0;
  }
  if (soPhut ==0 || soPhut == 60) {
    toaDoPhut =0;
  }
 
  var gocLech = Math.abs(toaDoGio- toaDoPhut);
  document.querySelector("#xuatGocLech").innerHTML= `<p>Góc lệch là: ${gocLech}&deg; </p>`;

}

